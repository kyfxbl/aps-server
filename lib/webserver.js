var net = require('net');
var fs = require("fs");
var cp = require('child_process');
var program = require('commander');
var _ = require('underscore');
var os = require('os');
var utils = require('./utils/utils');
var worker = require('./startexpress');

var PORT = 3000;
var GRACE_EXIT_TIME = 2000;
var self = this;

function output(str) {
    console.log(str);
}
function main(fn) {
    fn();
}

var exitTimer = null;
function aboutExit() {
    if (exitTimer) return;
    exitTimer = setTimeout(function () {
        output('master exit...');
        process.exit(0);
    }, GRACE_EXIT_TIME);
}
function startServer() {
    worker.main(self);
}

function initSysconfig(sysconfig) {

    if (sysconfig.clusterConfig) {
        self.PORT = sysconfig.clusterConfig.port;
        self.WORKER_NUMBER = sysconfig.clusterConfig.workerno;
    } else {
        output('cluster :' + program.clusterName + ' not in topo config!');
        return;
    }

    self.clusterConfig = sysconfig.clusterConfig;
    self.topoConfig = sysconfig.topoConfig;
    self.nodeId = sysconfig.nodeId;
    self.appdir = sysconfig.appdir;

    global["_g_topo"] = sysconfig.topoConfig;
    global["_g_clusterConfig"] = sysconfig.clusterConfig;

    output('listen on ' + self.clusterConfig.port + "...");
}

void main(function () {

    var sysconfig = utils.parseCommand();

    initSysconfig(sysconfig);

    if (sysconfig.clusterConfig) {
        startServer();
        output('master is ok');

        process.once("exit", function (code, signal) {
            console.log("process terminated with code: " + code);
        });
        process.once("uncaughtException", function (error) {
            console.log("process terminated with error: ");
            console.log(error);
            console.log(error.stack);
            aboutExit();
        });

    } else {
        output('master start fail...');
    }
});