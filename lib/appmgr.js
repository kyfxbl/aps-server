/**
 * 应用管理
 */
var fs = require("fs"),
    _ = require("underscore"),
    logger = require("./utils/logger").getLogger(),
    express = require('express'),
    passport = require('passport'),
    utils = require("./utils/utils"),
    requestFilter = require("./express-middleware/requestFilter");

function ApplicationManager(clusterConfig) {
    this.clusterConfig = clusterConfig;
    this.appdir = global.appdir;

    var applist = clusterConfig.applications
    if (_.isArray(applist)) {
        this.applist = applist;
    } else {
        this.applist = [];
    }
}
_.extend(ApplicationManager.prototype, {
    loadAppMiddleWare: loadAppMiddleWare,
    start: start,
    stop: stop,
    reload: reload
})

module.exports = {
    ApplicationManager: ApplicationManager,
    Application: Application
}


function loadAppMiddleWare(router){
    var that = this;
    _.each(this.applist, function (appName) {
        var application = new Application(that.clusterConfig,appName, router);

        application.loadAppMeta();
        application.loadMiddleWare();
        application.loadFilters();
    });
}

/**
 * 加载应用启动
 */
function start(router) {
    var that = this;
    logger.debug("服务应用开始加载");
    // 如果是开发环境，映射apk path
    if (global["_g_topo"].env == "dev") {
        router.use("/apk", express.static(that.appdir + "apk/"));
    }

    _.each(this.applist, function (appName) {
        var application = new Application(that.clusterConfig,appName, router);

        application.loadAppMeta();

        process.nextTick(function () {
            application.loadService();
        });

        // 只有开发环境才用node加载静态文件，生产环境通过nginx加载
        if (global["_g_topo"].env == "dev") {
            process.nextTick(function () {
                application.loadStatic();
            });
        }

        process.nextTick(function () {
            application.loadExpressView();
        });

        process.nextTick(function () {
            application.loadErrorhandler(that);
        });

        process.nextTick(function () {
            application.loadInitial(that);
        });
    });

    //监听所有应用启动状态，如果全部启动成功发布systemOnReady事件
}

/**
 * 停止应用
 */
function stop() {

}

function reload() {

}

function Application(clusterConfig,name, router) {

    this.clusterConfig = clusterConfig;
    this.appdir = global.appdir?global.appdir:"";
    this.service_doc = global.service_doc?global.service_doc:"svc";
    this.appName = name;
    this.router = router;
}


Application.prototype.loadAppMeta = function () {
    var that = this;

    if (global["_g_topo"].env == "dev") {
        if(that.appdir){
            that.appJsLoadPath = that.appdir+"src/" + that.appName;
            that.appPath = that.appdir+"src/" + that.appName;

        }else{
            that.appJsLoadPath = "../src/" + that.appName;
            that.appPath = __dirname +"/../src/" + that.appName;
        }


    } else {

        if(that.appdir){
            that.appJsLoadPath = that.appdir+"webapps/" + that.appName;
            that.appPath = that.appdir+"webapps/" + that.appName;
        }else{
            that.appJsLoadPath = "../webapps/" + that.appName;
            that.appPath = __dirname +"/../webapps"/ + that.appName;
        }
    }

    if (!fs.existsSync(that.appPath)) {
        console.log(that.appPath + " not exists!");
        return false;
    }



    that.METAINF = utils.readJsonFileSync(that.appPath + "/META-INF.json");

    return true;
}
Application.prototype.loadService = function () {

    var that = this;
    var serviceList = that.METAINF.services;
    if (that.METAINF && serviceList && _.isArray(serviceList)) {

        _.each(serviceList, function (service) {
            var impl = service.impl,
                method = service.method,
                path = service.path,
                name = service.name,
                middleware = service.middleware;

            var defaultServicePath = that.appJsLoadPath + "/service/";
            var filePath = impl.substring(0, impl.lastIndexOf(".")).replace(/\./, "/");
            var implFnName = impl.substring(impl.lastIndexOf(".") + 1);
            var implMd = require(defaultServicePath + filePath);// 这里执行了require，把portal/service/downloadService.js加载进来
            if (implMd && path && that.router[method]) {
                if (_.isFunction(implMd[implFnName]) || _.isArray(implMd[implFnName])) {

                    path = "/"+that.service_doc + path;
                    var handler = implMd[implFnName];
                    if(middleware){

                        var middlewareFilePath = middleware.substring(0, impl.lastIndexOf(".")).replace(/\./, "/");
                        var middlewareFnName = middleware.substring(middleware.lastIndexOf(".") + 1);
                        var middlewareModule = require(defaultServicePath + middlewareFilePath);
                        var middleHandler = middlewareModule[middlewareFnName];

                        if (service.auth == false) {
                            that.router[method].call(that.router, path, [middleHandler], handler);
                        } else if (service.auth == "yilos-session") {
                            that.router[method].call(that.router, path, [passport.authenticate('yilos-session', { session: false,login:service.login}), middleHandler], handler);
                        } else if (service.auth == "yilos-oauth-rbac") {
                            that.router[method].call(that.router, path, [passport.authenticate('yilos-oauth-rbac', { accesses:service.accesses}), middleHandler], handler);
                        } else if(service.auth === "nailstar-cms"){
                            that.router[method].call(that.router, path, [passport.authenticate('nailstar-cms', {}), middleHandler], handler);
                        } else if(service.auth === "yilos-sales"){
                            that.router[method].call(that.router, path, [passport.authenticate('yilos-sales', {}), middleHandler], handler);
                        } else {
                            that.router[method].call(that.router, path, [passport.authenticate('yilos-session', { session: true, passReqToCallback: true }), middleHandler], handler);
                        }

                    }else{
                        if (service.auth == false) {
                            that.router[method].call(that.router, path, handler);
                        } else if (service.auth == "yilos-session") {
                            that.router[method].call(that.router, path, passport.authenticate('yilos-session', { session: false,login:service.login}), handler);
                        }else if (service.auth == "yilos-oauth-rbac") {
                            that.router[method].call(that.router, path, passport.authenticate('yilos-oauth-rbac', {accesses:service.accesses}), handler);
                        }else if(service.auth === "nailstar-cms"){
                            that.router[method].call(that.router, path, passport.authenticate('nailstar-cms', {}), handler);
                        }else if(service.auth === "yilos-sales"){
                            that.router[method].call(that.router, path, passport.authenticate('yilos-sales', {}), handler);
                        }else {
                            that.router[method].call(that.router, path, passport.authenticate('yilos-session', { session: true, passReqToCallback: true }), handler);
                        }
                    }
                }
            } else {
                logger.warn(name + " register fail,path:" + path);
            }
        });
    }else{
        console.log(this.appName);
    }
}
Application.prototype.loadStatic = function () {
    var that = this;
    var staticList = that.METAINF.statics;
    if (staticList && _.isArray(staticList)) {
        _.each(staticList, function (staticd) {

            var path = staticd.path;
            var name = staticd.name;
            var dir = staticd.dir || "/static/";

            var staticResourceLocation = that.appPath + dir;
            if (path) {
                that.router.use(path, express.static(staticResourceLocation));
            } else {
                logger.warn(name + " register fail,dir: " + staticResourceLocation);
            }
        });
    }
}
/**
 * 注册每个应用中声明的express ejsView
 */
Application.prototype.loadExpressView = function () {
    var that = this;
    var ejsviewList = that.METAINF.ejsviews;
    if (ejsviewList && _.isArray(ejsviewList)) {
        _.each(ejsviewList, function (ejsd) {
            var dir = ejsd.dir;

            if (dir) {
                var views = that.router.get("views");
                if (_.isArray(views)) {
                    views.push( that.appPath + dir)
                }
                that.router.set('views', views);
            } else {
                logger.warn(dir + " register fail,dir:" +  that.appPath + dir);
            }
        });
    }
}

/**
 * 加载系统错误页面处理信息
 * <errorCode,errorHanderDef>
 * 将系统中所有加载模块的错误处理信息加载到ApplicationManager.errorHanderMap中
 */
Application.prototype.loadErrorhandler = function (ApplicationManager) {
    var that = this;
    var errorHandlers = that.METAINF.errorHandler;
    var errorMap = {

    };
    if (errorHandlers && _.isArray(errorHandlers)) {
        _.each(errorHandlers, function (errdef) {
            errorMap[errdef.errorCode] = errdef;
        });
    }
    if (!ApplicationManager.errorHanderMap) {
        ApplicationManager.errorHanderMap = {

        };
    }
    ApplicationManager.errorHanderMap = _.extend(ApplicationManager.errorHanderMap, errorMap);
}

/**
 * 加载模块初始化方法
 */
Application.prototype.loadMiddleWare = function () {
    var that = this;
    var loadMiddleWare = that.METAINF.loadMiddleWare;
    if(!loadMiddleWare){
        return;
    }

    var impl = loadMiddleWare.impl,
        name = loadMiddleWare.name;


    var defaultServicePath = that.appJsLoadPath + "/service/";

    var filePath = impl.substring(0, impl.lastIndexOf(".")).replace(/\./, "/");
    var implFnName = impl.substring(impl.lastIndexOf(".") + 1);

    var implMd = require(defaultServicePath + filePath);// 这里执行了require，把portal/service/downloadService.js加载进来
    if (implMd) {
        if (_.isFunction(implMd[implFnName])) {
            var handler = implMd[implFnName];
            handler.call({},that.router,that.clusterConfig);
        }
    } ;
}

Application.prototype.loadFilters = function() {
    var that = this;
    var filters = that.METAINF.filters;
    if(!filters){
        return;
    }

    var defaultServicePath = that.appJsLoadPath + "/service/";
    _.each(filters, function(filter){
        var beforeImpl = filter.beforeImpl;
        var beforeFilePath = beforeImpl.substring(0, beforeImpl.lastIndexOf(".")).replace(/\./, "/");
        var beforeImplFnName = beforeImpl.substring(beforeImpl.lastIndexOf(".") + 1);
        if(beforeImpl && beforeFilePath && beforeImplFnName){
            var beforeImplMd = require(defaultServicePath + beforeFilePath);
            if (_.isFunction(beforeImplMd[beforeImplFnName])) {
                var handler = beforeImplMd[beforeImplFnName];
                that.router.use(filter.path, handler);
            }
        }

        var afterImpl = filter.afterImpl;
        var afterFilePath = afterImpl.substring(0, afterImpl.lastIndexOf(".")).replace(/\./, "/");
        var afterImplFnName = afterImpl.substring(afterImpl.lastIndexOf(".") + 1);
        if(afterImpl && afterFilePath && afterImplFnName){
            var afterImplMd = require(defaultServicePath + afterFilePath);
            if (_.isFunction(afterImplMd[afterImplFnName])) {
                var handler = afterImplMd[afterImplFnName];
                requestFilter.responseFilters.push({path: filter.path, handler: handler});
            }
        }
    })
}

/**
 * 加载模块初始化方法
 */
Application.prototype.loadInitial = function (ApplicationManager) {
    var that = this;
    var initialScript = that.METAINF.initial;
    if(!initialScript){
        return;
    }

    var impl = initialScript.impl,
        name = initialScript.name;


    var defaultServicePath = that.appJsLoadPath + "/service/";

    var filePath = impl.substring(0, impl.lastIndexOf(".")).replace(/\./, "/");
    var implFnName = impl.substring(impl.lastIndexOf(".") + 1);

    var implMd = require(defaultServicePath + filePath);// 这里执行了require，把portal/service/downloadService.js加载进来
    if (implMd) {
        if (_.isFunction(implMd[implFnName])) {
            var handler = implMd[implFnName];
            handler.call({},that.router,that.clusterConfig);
        }
    } ;
}

