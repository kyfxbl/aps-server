var mysqlProxyC = require("./mysqlProxy");

var sqlproxyCache = {
};

var dbconfig= {

    "getSqlProxy": function (datasource) {
        if(!datasource){
            console.log("mysql data source not found");
            return null;
        }
        var datasourceTag = datasource.dbserver.host + ":" + datasource.dbserver.port + "/" + datasource.dbName;
        if (!sqlproxyCache[datasourceTag]) {
            sqlproxyCache[datasourceTag] = new mysqlProxyC(datasource);
        }
        return  sqlproxyCache[datasourceTag];
    },

    C: {
        "DEFAULT_SQLDB": {
            dbName: global["_g_topo"].dataServer.mysql.dbName || "planx_graph",
            dbserver: {
                host: global["_g_topo"].dataServer.mysql.ip,
                port: global["_g_topo"].dataServer.mysql.port,
                database: global["_g_topo"].dataServer.mysql.dbName || "planx_graph",
                user: global["_g_topo"].dataServer.mysql.user,
                password: global["_g_topo"].dataServer.mysql.password,
                charset: "UTF8MB4_GENERAL_CI"
            }
        }
    }
};

module.exports = dbconfig;
