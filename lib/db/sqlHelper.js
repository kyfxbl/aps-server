var _ = require("underscore");

exports.getInsertSqlOfObj = getInsertSqlOfObj;
exports.getUpdateSqlOfObjId = getUpdateSqlOfObjId;
exports.getDelSqlByObjId = getDelSqlById;

exports.getServerInsertForMysql = getServerInsertForMysql;
exports.getServerDelForMysql = getServerDelForMysql;

exports.getServerInsertSqlOfObj = getServerInsertSqlOfObj;
exports.getServerUpdateSqlOfObjId = getServerUpdateSqlOfObjId;


//获取一个insert语句及参数，fitterArray过滤数组
function getServerInsertSqlOfObj(dbName,tableName, obj, fitterArray,ignoreRepeat) {

    var field = "(";
    var values = "(";
    var valuesPlace = [];
    var insertObj;
    var key;
    var insertSql;

    insertObj = fitterArray ? fitterObjAttr(obj, fitterArray) : obj;
    for (key in insertObj) {
        if ( _.has(insertObj,key)  &&  !_.isNull(insertObj[key]) && !_.isUndefined(insertObj[key]) && insertObj[key]!=="") {
            if(key == "key"){
                field += "keyName,";
                values += ":keyName,";
            }else if(key == "desc"){
                field += "description,";
                values += ":description,";
            }else if(key == "describe"){
                field += "description,";
                values += ":description,";
            }else{
                field += key + ",";
                values += ":"+key+",";
            }
            valuesPlace.push(insertObj[key]);
        }
    }
    field = field.slice(0, field.length - 1) + ")";
    values = values.slice(0, values.length - 1) + ")";

    if (dbName) {
        insertSql = "insert "+(ignoreRepeat?" or ignore ":"")+" into " + dbName + "." + tableName + field + " values" + values + ";";
    }
    else {
        insertSql = "insert "+(ignoreRepeat?" or ignore ":"")+"into " + tableName + field + " values" + values + ";";
    }
    return {statement: insertSql, value: insertObj};
}

function getServerUpdateSqlOfObjId(dbName, tableName, updateObj, fitterArray) {
    var updateField = [];
    var updateSql;

    if (!_.has(updateObj, "id")) {
        return {statement: "", value: {}};
    }

    if (_.isArray(fitterArray) && !_.isEmpty(fitterArray)) {
        updateObj = _.pick(updateObj, fitterArray);
    }

    _.each(updateObj, function (value, key) {
        if (key === "id"){
            return;
        }

        if (_.isArray(value) || _.isObject(value)) {
            return;
        }
        updateField.push(key + " = :" + key);
    });

    if (dbName) {
        updateSql = "update " + dbName + "." + tableName + " set " + updateField.join(", ") + " where id = :id;";
    }
    else {
        updateSql = "update " + tableName + " set " + updateField.join(", ") + " where id = :id;";
    }

    return {statement: updateSql, value: updateObj};
}

//获取一个insert语句及参数，fitterArray过滤数组(写入mysql不带"or")
function getServerInsertForMysql(dbName,tableName, obj, fitterArray,ignoreRepeat) {

    var field = "(";
    var values = "(";
    var valuesPlace = [];
    var insertObj;
    var key;
    var insertSql;

    insertObj = fitterArray ? fitterObjAttr(obj, fitterArray) : obj;
    for (key in insertObj) {
        if ( _.has(insertObj,key)  &&  !_.isNull(insertObj[key]) && !_.isUndefined(insertObj[key]) && insertObj[key]!=="") {
            if(key == "key"){
                field += "keyName,";
                values += ":keyName,";
            }else if(key == "desc"){
                field += "description,";
                values += ":description,";
            }else if(key == "describe"){
                field += "description,";
                values += ":description,";
            }else{
                field += key + ",";
                values += ":"+key+",";
            }
            valuesPlace.push(insertObj[key]);
        }
    }
    field = field.slice(0, field.length - 1) + ")";
    values = values.slice(0, values.length - 1) + ")";

    if (dbName) {
        insertSql = "insert "+(ignoreRepeat? " ignore ":"")+" into " + dbName + "." + tableName + field + " values" + values + ";";
    }
    else {
        insertSql = "insert "+(ignoreRepeat? " ignore ":"")+"into " + tableName + field + " values" + values + ";";
    }
    return {statement: insertSql, value: insertObj};
}

function getServerDelForMysql(dbName, tableName, id){
    var deleteSql = "delete from " + dbName + "." + tableName +
        " where id = :id;";
    var id = {id: id};
    return {statement: deleteSql, value: id};
}


//获取一个insert语句及参数，fitterArray过滤数组
function getInsertSqlOfObj(dbName,tableName, obj, fitterArray,ignoreRepeat) {

    var field = "(";
    var values = "(";
    var valuesPlace = [];
    var insertObj;
    var key;
    var insertSql;

    insertObj = fitterArray ? fitterObjAttr(obj, fitterArray) : obj;
    for (key in insertObj) {
        if (_.has(insertObj,key) &&  !_.isNull(insertObj[key]) && !_.isUndefined(insertObj[key]) && insertObj[key]!=="") {
            if(key == "key"){
                field += "keyName,";
            }else if(key == "desc"){
                field += "description,";
            }else if(key == "describe"){
                field += "description,";
            }else{
                field += key + ",";
            }

            values += ":"+key+",";
            valuesPlace.push(insertObj[key]);
        }
    }
    field = field.slice(0, field.length - 1) + ")";
    values = values.slice(0, values.length - 1) + ")";

    if (dbName) {
        insertSql = "insert "+(ignoreRepeat?" or ignore ":"")+" into " + dbName + "." + tableName + field + " values" + values + ";";
    }
    else {
        insertSql = "insert "+(ignoreRepeat?" or ignore ":"")+"into " + tableName + field + " values" + values + ";";
    }
    return {statement: insertSql, value: valuesPlace};
}

//根据对象id获取update语句及参数
function getUpdateSqlOfObjId(dbName,tableName, obj, fitterArray) {

    var setFieldStr = "";
    var valuesPlace = [];
    var updateObj;
    var key;
    var updateSql;
    var id;

    updateObj = fitterArray ? fitterObjAttr(obj, fitterArray) : obj;

    for (key in updateObj) {
        if (_.has(updateObj,key)) {
            if (key === "id") {
                continue;
            }
            //排除复杂对象
            if(updateObj[key]==undefined ||  _.isObject(updateObj[key]) || _.isArray(updateObj[key])){
                continue;
            }
            setFieldStr += key + "=?,";
            valuesPlace.push(updateObj[key]);
        }
    }

    valuesPlace.push(updateObj.id);
    setFieldStr = setFieldStr.slice(0, setFieldStr.length - 1);

    if (dbName) {
        updateSql = "update " + dbName + "." + tableName + " set " + setFieldStr + " where id = ?;";
    }
    else {
        updateSql = "update " + tableName + " set " + setFieldStr + " where id = ?;";
    }

    return {statement: updateSql, value: valuesPlace};
}

//根据id获取SQL删除语句
function getDelSqlById(dbName, tableName, id) {
    var deleteSql = "";
    if (dbName) {
        deleteSql = "delete from " + dbName + "." + tableName + " where id=?;";
    }
    else {
        deleteSql = "delete from " + tableName + " where id=?;";
    }
    var values = [id];
    return {statement: deleteSql, value: values};
}

//过滤对象属性
function fitterObjAttr(obj, filterArray) {
    //_.pick(obj,fitterArray)
    var key, fitterResult = {};
    if (_.isArray(filterArray)) {
        for (key in obj) {
            if (_.has(obj,key) && filterArray.indexOf(key.toString()) !== -1) {
                if (_.isNumber(obj[key]) || _.isString(obj[key])) {
                    //默认""转换为NULL进行插入
                    if (obj[key] === "") {
                        fitterResult[key] = null;
                        continue;
                    }
                    fitterResult[key] = obj[key];
                }
            }
        }
    }
    return fitterResult;
}

