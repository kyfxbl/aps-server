var mysql = require('mysql');
var logger = require("../utils/logger").getLogger();

module.exports = MysqlProxy;

function MysqlProxy(datasource){
    this.databaseName = datasource.dbName;
    this.pool  = mysql.createPool(datasource.dbserver);
}

MysqlProxy.prototype = {
    getConn:getConn,
    execSql:execSql,
    bacthExecSql:bacthExecSql,
    bacthSeriesExecSql: bacthSeriesExecSql
};

function getConn(callback){
    this.pool.getConnection(function(err, conn) {
        if(err){
            logger.sqlError(err);
            callback(err,-1);
        }else{
            conn.config.queryFormat = function (query, values) {
                if (!values) return query;
                return query.replace(/\:(\w+)/g, function (txt, key) {
                    if (_.has(values,key)) {
                        return this.escape(values[key]);
                    }
                    return txt;
                }.bind(this));
            };
            //conn.query("SET character_set_client=utf8,character_set_connection=utf8");

            callback(null,conn);
        }
    });
}

function closeConn(conn){
    try{
        conn.release();
    }catch(e){
        logger.sqlError(e);
    }
}

function execSql (sql,option,callback){
    this.getConn(function(err, conn) {
        if(err){
            logger.error(err);
            closeConn(conn);
            callback(err,-1);
            return;
        }
        conn.query(sql,option,function(err,result){
            if(err){
                logger.sqlError(err);
                logger.sqlError(sql);
                logger.sqlError(option);
                closeConn(conn);
                err.code = err.errno;
                callback(err,result);
                return;
            }
            closeConn(conn);
            callback(null,result);
        });
    });
}

function bacthExecSql(sqlArray,callback){
    this.getConn(function(err, conn) {
        conn.beginTransaction(function (err) {
            if(err) {
                logger.sqlError(err);
                closeConn(conn);
                callback(error);
            }
            async.eachLimit(sqlArray, 10, function (item, sub_callback) {
                conn.query(item.statement, item.value, function (error) {
                    if (error) {
                        logger.sqlError(error);
                        logger.sqlError(item.statement);
                        sub_callback({error: error});
                        return;
                    }
                    sub_callback(null);
                });
            }, function (error) {
                if (error) {
                    logger.sqlError(error);

                    conn.rollback(function () {
                        closeConn(conn);
                        callback(error);
                    });
                } else {
                    conn.commit(function (err) {
                        if (err) {
                            logger.sqlError(err);
                            conn.rollback(function () {
                                closeConn(conn);
                                callback(err);
                            });
                        } else {
                            closeConn(conn);
                            callback(null);
                        }
                    });

                }
            });
        });
    });
}

function bacthSeriesExecSql(sqlArray,callback){
    this.getConn(function(err, conn) {
        conn.beginTransaction(function (err) {
            if(err) {
                logger.sqlError(err);
                closeConn(conn);
                callback(error);
            }
            async.eachSeries(sqlArray, function (item, sub_callback) {
                conn.query(item.statement, item.value, function (error) {
                    if (error) {
                        logger.sqlError(error);
                        sub_callback({error: error});
                        return;
                    }
                    sub_callback(null);
                });
            }, function (error) {
                if (error) {
                    logger.sqlError(error);

                    conn.rollback(function () {
                        closeConn(conn);
                        callback(error);
                    });
                } else {
                    conn.commit(function (err) {
                        if (err) {
                            logger.sqlError(err);

                            conn.rollback(function () {
                                closeConn(conn);
                                callback(err);
                            });
                        } else {
                            closeConn(conn);
                            callback(null);
                        }
                    });
                }
            });
        });
    });
}