global.FRAMEWORKPATH = __dirname;
global.doError = doError;
global.doResponse = doResponse;
global.doRender = doRender;
global.logRequest = logRequest;
global.assembleResultObject = assembleResultObject;
global.wrapResult = assembleResultObject;
global.passport = require('passport');
global._ = require("underscore");
global.async = require("async");

var logger = require("./utils/logger").getLogger();
var utils = require("./utils/utils")
    , requestFilter = require("./express-middleware/requestFilter");

function doError(callback, err) {
    logger.error(err);
    callback(err);
}

function doResponse(req, res, jsonObj) {
    if(requestFilter.responseFilters){
        async.each(requestFilter.responseFilters, function(filter, callback){
            if(req.url.indexOf(filter.path) == 0){
                filter.handler(req, res, jsonObj, callback);
            }
            else{
                callback(null);
            }
        }, function(error){
            if(error){
                doResponseWrite(req, res, assembleResultObject(error));
            }
            else{
                doResponseWrite(req, res, jsonObj);
            }
        })
    }
    else{
        doResponseWrite(req, res, jsonObj);
    }
}
function doResponseWrite(req,res, jsonObj){
    var executeTime = new Date().getTime() - res.getHeader("beginTime");
    var requestId = res.getHeader("requestId");
    if(!jsonObj){
        jsonObj = {
            code:0,
            result:{}
        }
    }else if(jsonObj && jsonObj.code == undefined){
        jsonObj = {
            code:0,
            result:jsonObj
        }
    }
    var jsons = JSON.stringify(jsonObj);
    var statusCode = 200;
    if(res.statusCode){
        statusCode = res.statusCode;
    }
    if(executeTime>2000){
        logger.error("execute slow request,executeTime:" + executeTime);
        logRequest(req, res);
    }
    res.writeHead(statusCode, {'executeTime': executeTime, 'Content-Type': "application/json;charset=UTF-8"});
    res.end(jsons);
}

function doRender(req,res,data,renderTemplate,renderLayout){
    if(utils.isXhr(req)){
        res.json(data);
    }else{
        var redirectTo = req.headers["redirectTo"];
        if(redirectTo){
            res.redirect(redirectTo);
        }else {
            res.render(renderTemplate, _.extend({layout:(renderLayout)?renderLayout:"none"},data));
        }
    }
}


function logRequest(req, res) {
    if (req) {
        logger.error("request body:" + JSON.stringify(req.body));
        logger.error("request header:" + JSON.stringify(req.headers));
        logger.error("request params:" + JSON.stringify(req.params));
        logger.error("request query:" + JSON.stringify(req.query));
    }
    if (res) {
        logger.error("response.statusCode:" + res.statusCode);
        logger.error("response body:" + JSON.stringify(res.body));
        logger.error("response header:" + JSON.stringify(res.headers));
        logger.error("response params:" + JSON.stringify(res.params));
        logger.error("response query:" + JSON.stringify(res.query));
    }
}


function assembleResultObject(err, result) {

    var response = {};
    if (err) {
        response.code = 1;
        response.error = err
    } else {
        response.code = 0;
        response.result = result;
    }
    return response;
}
