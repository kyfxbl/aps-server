/**
 * Created with JetBrains WebStorm.
 * User: ganyue
 * Date: 14-7-31
 * Time: 下午3:20
 * To change this template use File | Settings | File Templates.
 */

exports.init = init;
var ejs = require('ejs');
var moment = require('moment');

function init(app) {
    app.locals.dateFormat = function (obj, format) {
        if (!obj) {
            return "";
        }
        if (format == undefined) {
            format = 'YYYY-MM-DD HH:mm:ss';
        }
        var ret;
        if (_.isDate(obj)) {
            ret = moment(obj, undefined, "zh_cn").format(format);
        } else if (IsNum(obj)) {
            ret = moment(Number(obj)).format(format);
        } else {
            ret = moment(obj, format, "zh_cn").format(format);
            if (ret == 'Invalid date') {
                ret = moment(obj, undefined, "zh_cn").format(format);
            }
        }
        return ret == 'Invalid date' ? '0000-00-00 00:00:00' : ret;
    };

    app.locals.toJson = function (obj) {
        return JSON.stringify(obj);
    };

    app.locals.statusTransfer = function (status) {
        return status === 1 ? "待开通" : "已开通";
    };

    app.locals.staticUrl = function (url) {
        if (global["_g_topo"].env == "production") {
            return "http://st1.yilos.com" + url;

        } else {
            return url;
        }
    };

    app.locals.hasMenu = function (menus, menuAccessCode) {
        return _.find(menus, function (menu) {
            return menu == menuAccessCode;
        });
    }
}

function IsNum(s) {
    if (s != null) {
        if (_.isNumber(s)) {
            return true;
        } else {
            var re = /\d*/i; //\d表示数字,*表示匹配多个数字
            var r = s.match(re);
            return (r == s) ? true : false;
        }
    }
    return false;
}