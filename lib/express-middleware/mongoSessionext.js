var express = require('express'),
    MongoStore = require('connect-mongo')(express);
    mongodb = require("mongodb"),
    ReplSetServers = mongodb.ReplSetServers,
    Server = mongodb.Server;

module.exports = {
    getMongoStore:getMongoStore
}


function getMongoStore(dbName){
    var nosqlProxyFC = require(FRAMEWORKPATH +"/db/dataProxyFC");
    var serverArray = [];
    var mongoServer = nosqlProxyFC.C.DEFAULT_NOSQLDB.dbserver;
    if(_.contains(mongoServer.port,",")){
        var ports = mongoServer.port.split(",");
        for(var i=0;i<ports.length;i++){
            serverArray.push(new Server(mongoServer.host, ports[i]));
        }
    }else{
        serverArray.push(new Server(mongoServer.host, mongoServer.port));
    }
    var replSet = new ReplSetServers(serverArray);


    var db_connector = new mongodb.Db(dbName, replSet, {});

    return new MongoStore({
        db: db_connector
    });
}