/**
 * Created with JetBrains WebStorm.
 * User: ganyue
 * Date: 14/11/10
 * Time: 下午12:03
 * To change this template use File | Settings | File Templates.
 */
/**
 * Copyright 2013-present NightWorld.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var error = OAuth2Error,
    runner = require('./runner');
var util = require('util');
var utils = require("../utils/utils");
var dbHelper = require(FRAMEWORKPATH + "/utils/dbHelper");
module.exports = Authorise;

/**
 * This is the function order used by the runner
 *
 * @type {Array}
 */
var fns = [
    getBearerToken,
    checkToken
];

/**
 * Authorise
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
function Authorise (req, res, next) {
    this.req = req;
    utils.runner(fns, this, next);
}

/**
 * Get bearer token
 *
 * Extract token from request according to RFC6750
 *
 * @param  {Function} done
 * @this   OAuth
 */
function getBearerToken (done) {
    var headerToken = this.req.get('Authorization'),
        getToken =  this.req.query.access_token,
        postToken = this.req.body ? this.req.body.access_token : undefined;

    // Check exactly one method was used
    var methodsUsed = (headerToken !== undefined) + (getToken !== undefined) +
        (postToken !== undefined);

    if (methodsUsed > 1) {
        return done(error('invalid_request',
            'Only one method may be used to authenticate at a time (Auth header,  ' +
                'GET or POST).'));
    } else if (methodsUsed === 0) {
        return done(error('invalid_request', 'The access token was not found'));
    }

    // Header: http://tools.ietf.org/html/rfc6750#section-2.1
    if (headerToken) {
        var matches = headerToken.match(/Bearer\s(\S+)/);

        if (!matches) {
            return done(error('invalid_request', 'Malformed auth header'));
        }

        headerToken = matches[1];
    }

    // POST: http://tools.ietf.org/html/rfc6750#section-2.2
    if (postToken) {
        if (this.req.method === 'GET') {
            return done(error('invalid_request',
                'Method cannot be GET When putting the token in the body.'));
        }

        if (!this.req.is('application/x-www-form-urlencoded')) {
            return done(error('invalid_request', 'When putting the token in the ' +
                'body, content type must be application/x-www-form-urlencoded.'));
        }
    }

    this.bearerToken = headerToken || postToken || getToken;
    done();
}

/**
 * Check token
 *
 * Check it against ensure it's not expired
 * @param  {Function} done
 * @this   OAuth
 */
function checkToken (done) {
    var self = this;
   getAccessToken(this.bearerToken, function (err, token) {
        if (err) return done(error('server_error', false, err));

        if (!token) {
            return done(error('invalid_token',
                'The access token provided is invalid.'));
        }

        if (token.expires !== null &&
            (!token.expires || token.expires < new Date())) {
            return done(error('invalid_token',
                'The access token provided has expired.'));
        }

        // Expose params
        self.req.oauth = { bearerToken: token };
        self.req.user = token.user ? token.user : { id: token.userId };

        done();
    });
}


/**
 * Error
 *
 * @param {Number} code        Numeric error code
 * @param {String} error       Error descripton
 * @param {String} description Full error description
 */
function OAuth2Error (error, description, err) {
    if (!(this instanceof OAuth2Error))
        return new OAuth2Error(error, description, err);

    Error.call(this);

    this.name = this.constructor.name;
    if (err instanceof Error) {
        this.message = err.message;
        this.stack = err.stack;
    } else {
        this.message = description;
        Error.captureStackTrace(this, this.constructor);
    }

    switch (error) {
        case 'invalid_client':
            this.headers = {
                'WWW-Authenticate': 'Basic realm="Service"'
            };
        /* falls through */
        case 'invalid_grant':
        case 'invalid_request':
            this.code = 400;
            break;
        case 'invalid_token':
            this.code = 401;
            break;
        case 'server_error':
            this.code = 503;
            break;
        default:
            this.code = 500;
    }

    this.error = error;
    this.error_description = description || error;
}

util.inherits(OAuth2Error, Error);



/*
 * Required
 */

function getAccessToken(bearerToken, callback) {
    dbHelper.execSql('SELECT access_token, client_id, expires, user_id FROM oauth_access_tokens ' +
        'WHERE access_token = :access_token', {access_token:bearerToken}, function (err, result) {
        if (err || !result.rowCount) return callback(err);
        // This object will be exposed in req.oauth.token
        // The user_id field will be exposed in req.user (req.user = { id: "..." }) however if
        // an explicit user object is included (token.user, must include id) it will be exposed
        // in req.user instead
        var token = result.rows[0];
        callback(null, {
            accessToken: token.access_token,
            clientId: token.client_id,
            expires: token.expires,
            userId: token.userId
        });
    });
};