/**
 * 系统当前支持的安全策略定义
 */
var passport = require('passport')
    , YLSSessionStrategy = require('./passport-extension/YLSSessionStrategy')
    , YLSOauthRbacStrategy = require('./passport-extension/YLSOauthRbacStrategy')
    , YLSNailstarStrategy = require("./passport-extension/YLSNailstarStrategy")
    , YLSSalesStrategy = require("./passport-extension/YLSSalesStrategy")
    , _ = require("underscore")
    , dbHelper = require( "../utils/dbHelper")
    ,logger = require("../utils/logger").getLogger()
    , utils = require("../utils/utils");


/**
 * YLSStrategy
 * This strategy is used to authenticate users based on an access token (aka a
 * bearer token).  The user must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */

exports.initStrategy = function(){
    passport.use(new YLSSessionStrategy({}));
    passport.use(new YLSOauthRbacStrategy({}));
    passport.use(new YLSNailstarStrategy({}));
    passport.use(new YLSSalesStrategy({}));
}


