/**
 * 系统当前支持的安全策略定义
 */
var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
    , _ = require("underscore")
    ,logger = require("../../utils/logger").getLogger()
    , utils = require("../../utils/utils")
    , dbHelper = require( "../../utils/dbHelper");

/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */

passport.use(new LocalStrategy(
    function (username, password, done) {
        dbHelper.queryData("tb_users", {username:username}, function(error, users){
            if(error){
                return done(null, false);
            }else{
                if(users.length <= 0){
                    return done(null, false);
                }else{
                    var user = users[0];
                    if (user.password != utils.md5(password)) {
                        return done(null,false,{errorCode:"10050002"});
                    }
                    return done(null, user);
                }
            }
        });
    }
));

