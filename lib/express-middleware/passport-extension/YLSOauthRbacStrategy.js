/**
 * Module dependencies.
 */
var passport = require('passport')
    , util = require('util')
    , utils = require("../../utils/utils")
    , dbHelper = require("../../utils/dbHelper");


/**
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options) {
    if (typeof options == 'function') {
        verify = options;
        options = {};
    }
    passport.Strategy.call(this);
    this.name = 'yilos-oauth-rbac';
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a HTTP Bearer authorization
 * header, body parameter, or query parameter.
 *
 * @param {Object} req
 * @api protected
 */
Strategy.prototype.authenticate = function (req, options) {
    var self = this;
    var token = undefined;
    if (req.headers && req.headers['authorization']) {
        var parts = req.headers['authorization'].split(' ');
        if (parts.length == 2) {
            var scheme = parts[0]
                , credentials = parts[1];
            if (/Bearer/i.test(scheme)) {
                token = credentials;
            }
        }
    }
    //当前第一个版本内部服务调用使用内部token，通过IP地址来限制外部调用
    if (token == "SUPERTOKEN" && global["_g_topo"]["ip-whitelist"].indexOf(req.connection.remoteAddress) != -1) {
        self.pass();
        return;
    }

    if (!req.session || !req.session.expires_in > (new Date(this.now)).getTime() || _.isEmpty(req.session.username)) {
        //根据子系统判断跳转登录界面
        var login = options.login;
        if (login) {
            if (utils.isXhr(req)) {
                return self.fail(401);
            } else {
                return this.redirect(login);
            }
        } else {
            if (utils.isXhr(req)) {
                return self.fail(401);
            } else {
                return this.redirect("http://www.yilos.com");
            }
        }
    }

    //检查用户是否有调用该接口权限
    if (_.isEmpty(options.accesses)) {
        self.pass();
        return;
    }
    _checkAccess(req.session.username, function (error, passFlag) {
        if (error || !passFlag) {
            return self.fail(403);
        } else {
            self.pass();
        }
    });

    function _checkAccess(userName, callback) {
        if (_.isEmpty(userName)) {
            callback({code: 1, errorMsg: "参数userName为空"}, false);
            return;
        }
        var passFlag = true;
        async.series([_checkIsChainAdmin, _checkUserAccesses], function (error) {
            if (error) {
                console.error("查询用户是否有调用接口权限失败，userName：" + userName + "，error：" + error);
            }
            callback(error, passFlag);
        });

        function _checkIsChainAdmin(callback) {
            var sql = "select roleId from tb_users where username = :userName";
            dbHelper.execSql(sql, {userName: userName}, function (error, result) {
                if (error) {
                    console.error("根据用户账号查询角色失败，userName：" + userName + "error：" + error);
                    callback(error);
                    return
                }
                passFlag = _.isEmpty(result) ? false : (result[0].roleId == "chain.admin");
                callback(null);
            });
        }

        function _checkUserAccesses(callback) {
            if (passFlag) {
                callback(null);
                return;
            }
            passFlag = true;
            var sql = "select a.code from tb_user_role ur, tb_role_access ra, tb_access a " +
                "where ur.role_id = ra.role_id and ra.access_id = a.id and ur.master_id = ra.master_id and ur.user_name = :userName";
            dbHelper.execSql(sql, {userName: userName}, function (error, result) {
                if (error) {
                    console.error("根据用户账号查询角色失败，userName：" + userName + "error：" + error);
                    callback(error);
                    return
                }
                var userAccesses = result || [];
                for (var i = 0; i < options.accesses.length; i++) {
                    var access = _.find(userAccesses, function (item) {
                        return options.accesses[i] == item.code;
                    });
                    if (!access) {
                        passFlag = false;
                        break;
                    }
                }
                callback(null);
            });
        }
    }
}


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
