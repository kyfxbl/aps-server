/**
 * Module dependencies.
 */
var passport = require('passport')
    , util = require('util')
    , utils = require( "../../utils/utils")
    , dbHelper = require( "../../utils/dbHelper");


/**
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options) {
    if (typeof options == 'function') {
        verify = options;
        options = {};
    }
    passport.Strategy.call(this);
    this.name = 'yilos-session';
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a HTTP Bearer authorization
 * header, body parameter, or query parameter.
 *
 * @param {Object} req
 * @api protected
 */
Strategy.prototype.authenticate = function(req,options) {

    var self = this;
    var token = undefined;
    if (req.headers && req.headers['authorization']) {
        var parts = req.headers['authorization'].split(' ');
        if (parts.length == 2) {
            var scheme = parts[0]
                , credentials = parts[1];
            if (/Bearer/i.test(scheme)) {
                token = credentials;
            }
        }
    }
    //当前第一个版本内部服务调用使用内部token，通过IP地址来限制外部调用
    if(token =="SUPERTOKEN" && global["_g_topo"]["ip-whitelist"].indexOf(req.connection.remoteAddress)!=-1){
        self.pass();
        return;
    }

    if (!req.session ||  !req.session.username) {
        //根据子系统判断跳转登录界面
        var login = options.login;
        if(login){
            return this.redirect(login);
        } else{
            if(utils.isXhr(req)){
                return self.fail(401);
            }else{
                return this.redirect("http://www.yilos.com");
            }

        }
    }

    if (!req.session ||  !req.session.expires_in > (new Date(this.now)).getTime()) {
        //根据子系统判断跳转登录界面
        var login = options.login;
        if(login){
            return this.redirect(login);
        } else{
            if(utils.isXhr(req)){
                return self.fail(401);
            }else{
                return this.redirect("http://www.yilos.com");
            }

        }
    }


    if (req.session.single_chain && req.session.single_chain == "chain"){
        chainDbHelper.queryData("tb_users", {username:req.session.username}, function(error, users){
            if(error){
                return self.fail(401);
            }else{
                if(users.length <= 0){
                    return self.fail(401);
                }else{
                    self.pass();
                }
            }
        });
    }else{
        dbHelper.queryData("tb_users", {username:req.session.username}, function(error, users){
            if(error){
                return self.fail(401);
            }else{
                if(users.length <= 0){
                    return self.fail(401);
                }else{
                    self.pass();
                }
            }
        });
    }
}


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
