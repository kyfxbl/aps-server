/**
 * 针对只允许移动应用中访问HTTPS接口，进行接口访问控制
 * User: ganyue
 * Date: 14-8-6
 * Time: 下午7:17
 * To change this template use File | Settings | File Templates.
 */

var passport = require('passport')
    , util = require('util');


/**
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options) {
    if (typeof options == 'function') {
        verify = options;
        options = {};
    }
    passport.Strategy.call(this);
    this.name = 'yilos-apponly';
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a HTTP Bearer authorization
 * header, body parameter, or query parameter.
 *
 * @param {Object} req
 * @api protected
 */
Strategy.prototype.authenticate = function(req,options) {
    var self = this;
    //检查User-Agent，只允许YILOS APP发送多来的请求允许请求
    var userAgent = req.headers["user-agent"];
    if (userAgent.indexOf("NailShop") == -1) {
        //禁止访问
        return self.fail(401);
    }else{
        self.pass();
    }
}


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;