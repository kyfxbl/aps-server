var _ = require("underscore"),
    mysqlHelper = require( "../../db/mysqlHelper"),
    utils = require( "../../utils/utils"),
    logger = require("../../utils/logger").getLogger();


module.exports = {
    list: list,
    create: create,
    remove: remove,
    updateInit: updateInit,
    update: update,
    detail: detail
}

function list(schema,condition,model,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/list";
    _response(_.extend(model,{_list:[]}),renderTemplate ,req,res)
//    mysqlHelper.queryData(schema.table,condition,function(error,records){
//       if(error){
//           if (error) {
//               logger.error("列表界面展示错误table : "+schema.table+"，error：" + error);
//               next(error);
//               return;
//           }
//       }else{
//           _response(_.extend(model,{_list:records}) ,req,res)
//       }
//    });
}
function create(schema,model,record,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/add";
    mysqlHelper.addData(schema.table,record,function(error){
        if(error){
            if (error) {
                logger.error("列表新增展示错误table : "+schema.table+"，error：" + error);
                next(error);
                return;
            }
        }else{
            _response(_.extend(model,{_record:record}),renderTemplate ,req,res)
        }
    });
}
function remove(schema,model,condition,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/remove";
    var _condition = condition || {id:model.id};
    mysqlHelper.deleteDataByCondition(schema.table,_condition,function(error){
        if(error){
            if (error) {
                logger.error("列表删除展示错误table : "+schema.table+"，error：" + error);
                next(error);
                return;
            }
        }else{
            _response(model ,renderTemplate,req,res)
        }
    });
}

function updateInit(schema,model,condition,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/updateInit";
    var _condition = condition || {id:model.id};
    mysqlHelper.queryData( schema.table, _condition, function(error,records){
        if(error){
            if (error) {
                logger.error("列表修改初始化错误table : "+schema.table+"，error：" + error);
                next(error);
                return;
            }
        }else{
            _response(_.extend(model,{detail:records[0]}) ,renderTemplate,req,res)
        }
    });
}

function update(schema,model,record,condition,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/update";
    var _condition = condition || {id:model.id};
    mysqlHelper.update(_condition, schema.table, record,function(error){
        if(error){
            if (error) {
                logger.error("列表修改错误table : "+schema.table+"，error：" + error);
                next(error);
                return;
            }
        }else{
            _response(model ,renderTemplate,req,res)
        }
    });
}
function detail(schema,model,condition,req, res, next){
    var renderTemplate = req.headers["renderTemplate"] || "crud/detail";
    var _condition = condition || {id:model.id};
    mysqlHelper.queryData( schema.table, _condition,function(error,records){
        if(error){
            if (error) {
                logger.error("数据详情初始化错误table : "+schema.table+"，error：" + error);
                next(error);
                return;
            }
        }else{
            _response(_.extend(model,{detail:records[0]}) ,renderTemplate,req,res)
        }
    });
}

function _response(data,renderTemplate,req,res){
    if(utils.isXhr(req)){
        res.json(data);
    }else{
        var redirectTo = req.headers["redirectTo"];
        var renderLayout = req.headers["renderLayout"];
        if(redirectTo){
            res.redirect(redirectTo);
        }else {
            res.render(renderTemplate,{model:data,layout:(renderLayout)?renderLayout:"none"});
        }
    }
}