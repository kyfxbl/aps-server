var flash = require('connect-flash'),
    timeout = require('./express-middleware/connect-timeout'),
    express = require('express'),
    expressext = require("./express-middleware/expressext"),
    mongosessionext = require("./express-middleware/mongoSessionext"),
    passport = require('passport'),
    _ = require('underscore'),
    partials = require('./express-middleware/partials'),
    Loader = require('./express-middleware/staticLoader'),
    appmgr = require('./appmgr'),
    request = require('request'),
    utils = require('./utils/utils'),
    times = require('./utils/times'),
    ejsExtend = require('./ejs-extension/ejsExtend'),
    globalext = require('./globalext'),     //全局变量定义，不能删除
    logger = null,
    self = this;

var domain = require('domain');

exports.main = main;

function output(str) {
    console.log(str);
}

var app = null;

function allowCrossDomain(clusterConfig) {
    if(clusterConfig && clusterConfig.crossDomain){
        return function(req, res, next){
            res.setHeader('Access-Control-Allow-Origin', clusterConfig.crossDomain);
            res.setHeader('Access-Control-Allow-Credentials', true);
            res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
            res.setHeader('Access-Control-Allow-Headers', 'Authorization,Origin, Accept, Content-Type, X-HTTP-Method, X-HTTP-METHOD-OVERRIDE,XRequestedWith,X-Requested-With,xhr,custom-enterpriseId,x-clientappversion, x-wxopenid, x-devicetype');
            res.setHeader('Access-Control-Max-Age', '10');
            if ('OPTIONS' == req.method) {
                res.end("POST, GET, PUT, DELETE");
            }
            else {
                next();
            }
        }
    }
    else{
        return function(req, res, next){
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Credentials', true);
            res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
            res.setHeader('Access-Control-Allow-Headers', 'Authorization,Origin, Accept, Content-Type, X-HTTP-Method, X-HTTP-METHOD-OVERRIDE,XRequestedWith,X-Requested-With,xhr,custom-enterpriseId,x-clientappversion, x-wxopenid, x-devicetype');
            res.setHeader('Access-Control-Max-Age', '10');
            if ('OPTIONS' == req.method) {
                res.end("POST, GET, PUT, DELETE");
            }
            else {
                next();
            }
        }
    }
};

function statistics(req, res, next) {
    var beginTime = (new Date()).getTime();
    var requestId = nodeID + "-" + beginTime;
    res.setHeader("beginTime", beginTime);
    res.setHeader("requestId", requestId);
    if (req.user) {
        res.writeHead(200, {'USERNAME': req.user.username});
    }
    next();
}

function main (config) {

    global.nodeID = config.nodeId;
    global.appdir = config.appdir;
    global.service_doc = config.topoConfig.service_doc?config.topoConfig.service_doc:"svc";

    require("./utils/logger").initLogger();
    logger = require("./utils/logger").getLogger();
    app = express();

    // 覆盖express的视图查找逻辑，使其支持多个视图文件
    // 用以支持每个应用可以指定自己的express视图目录
    expressext.enableMultipleViewFolders(app.settings);
    if(config.topoConfig.env == "dev"){
        app.set('views', [config.appdir + 'src/ejs-template']);
    }else{
        app.set('views', [config.appdir + 'webapps/ejs-template']);
    }
    app.set('view engine', 'ejs');
    app.use(partials());
    app.locals({
        config: self.clusterConfig,
        Loader: Loader,
        env: config.topoConfig.env
    });

    start (config);

    function start (config) {
        self.clusterConfig = config.clusterConfig;
        self.topoConfig = config.topoConfig;
        //设置全局配置信息
        var appManager = new appmgr.ApplicationManager(self.clusterConfig);
        initExpress(appManager);
        appManager.start(app);
        app.use(function (err, req, res, next) {

            var current = (new Date());

            console.log(current)
            console.log(err)

            logger.error(JSON.stringify(err));
            var errHander = appManager.errorHanderMap[err.errorCode];
            if (errHander) {
                if (!errHander.returnTo) {
                    errHander.returnTo = global["_g_clusterConfig"].baseurl + "/portal/index.html";
                    errHander.returnToMsg = "返回登录";
                }
                if (utils.isXhr(req)) {
                    res.json(wrapError(err, errHander));
                } else {
                    if (errHander.templ) {
                        res.render(errHander.templ, _.extend(err, errHander));
                    } else {
                        res.end(JSON.stringify(wrapError(err, errHander)));
                    }

                }
            } else {
                if (utils.isXhr(req)) {
                    res.json(wrapError(err, errHander));
                } else {
                    res.render("error", {error: err,layout:"error_layout"});
                }

            }
            function wrapError(err, errHander) {
                var responseObject = err;
                if (err && err.code == undefined) {
                    responseObject = {
                        code: 1,
                        error: _.extend(err, errHander)
                    }
                }
                return responseObject;
            }
        });
        app.listen(self.clusterConfig.port);
        output('worker listen ok');
    }
    function initExpress(appManager) {

        var  maxAge =  5 * 60 * 60 * 1000 ;
        var  secret = '1234567890QWERTY';


        app.use(express.bodyParser({
                uploadDir: config.appdir + 'data/uploads',
                keepExtensions: true,
                limit: 200 * 1024 * 1024,// 100M limit
                defer: true//enable event
            }))
            .use('/'+global.service_doc+'/public', express.static(config.appdir + 'data'))
            .use(express.compress())
            .use(allowCrossDomain(appManager.clusterConfig))
            .use("/"+global.service_doc, express.cookieParser())
            .use("/oauth", express.cookieParser())
            .use(express.methodOverride())
            .use(flash())
            .use(timeout({
                time:30*60*1000
            }));

        app.use("/"+global.service_doc, express.session({   //超时时间设置，设置为不超时
            cookie: {
                maxAge: maxAge
            },
            secret: secret
        })).use("/oauth", express.session({   //超时时间设置，设置为不超时
            cookie: {
                maxAge: maxAge
            },
            secret: secret
        }));

        app.use(passport.initialize())
            .use(statistics)
            .use(passport.session());
        appManager.loadAppMiddleWare(app);

        app.get('/favicon.ico', function (req, res, next) {
            res.end("");
        });
        app.get('/'+global.service_doc+'/', function (req, res, next) {
            res.end("");
        });


        ejsExtend.init(app);
        var passportStrategy = require('./express-middleware/passportStrategy');
        passportStrategy.initStrategy();
        //发送启动短信
        if(self.topoConfig.env == "production"){
            process.env.NODE_ENV === "production";
        }
    }
}

