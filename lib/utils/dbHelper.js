var request = require("request");
var dataProxyFC  = require("../db/dataProxyFC");
var mysqlProxy = dataProxyFC.getSqlProxy(dataProxyFC.C.DEFAULT_SQLDB);

module.exports = {
    execSql:execSql,
    bacthExecSql:bacthExecSql,
    bacthSeriesExecSql:bacthSeriesExecSql
};

function execSql(sql,option,callback){
    mysqlProxy.execSql(sql,option,callback);
}

function bacthExecSql(sqlArray,callback){
    mysqlProxy.bacthExecSql(sqlArray,callback);
}

function bacthSeriesExecSql(sqlArray,callback){
    mysqlProxy.bacthSeriesExecSql(sqlArray,callback);
}