module.exports.getLogger = getLogger;
module.exports.initLogger = initLogger;

function getLogger(){
    return wrap;
}

var log4js = require('log4js');

function initLogger(){
    log4js.configure({
        appenders: [
            {
                type: 'file',
                filename: global.appdir+'logs/access_'+global["_g_clusterConfig"].ip+"_"+global["_g_clusterConfig"].port+'.log',
                maxLogSize: 2 * 1024 * 1024,
                backups: 3,
                category: 'access'
            },
            {
                type: "file",
                filename: global.appdir+"logs/bootstrap_"+global["_g_clusterConfig"].ip+"_"+global["_g_clusterConfig"].port+".log",
                maxLogSize: 2 * 1024 * 1024,
                backups: 3,
                category: 'bootstrap'
            },
            {
                type: "file",
                filename: global.appdir+"logs/sql_"+global["_g_clusterConfig"].ip+"_"+global["_g_clusterConfig"].port+".log",
                maxLogSize: 2 * 1024 * 1024,
                backups: 3,
                category: 'sql'
            },
            {
                type: "file",
                filename: global.appdir+"logs/error_"+global["_g_clusterConfig"].ip+"_"+global["_g_clusterConfig"].port+".log",
                maxLogSize: 2 * 1024 * 1024,
                backups: 3,
                category: 'error'
            }
        ]
    });
}


var logger = log4js.getLogger('access');
logger.setLevel('DEBUG');

var logger2 = log4js.getLogger("bootstrap");
logger2.setLevel("INFO");

var logger3 = log4js.getLogger("sql");
logger3.setLevel("INFO");

var logger4 = log4js.getLogger("error");
logger4.setLevel("INFO");


var InnerLogger = function(){
}

InnerLogger.prototype.error = function(){
    logger4.error.call(logger4,arguments);

}

InnerLogger.prototype.info = function(){
    logger.info.call(logger, arguments);
}

InnerLogger.prototype.debug = function(){
    logger.debug.call(logger, arguments);
}

InnerLogger.prototype.warn = function(){
    logger.warn.call(logger, arguments);
}

InnerLogger.prototype.isLevelEnabled = function(){
    logger.isLevelEnabled.call(logger, arguments);
}

InnerLogger.prototype.bootstrap = function(){
    logger2.info.call(logger2, arguments);
}
InnerLogger.prototype.sqlError = function(){
    logger3.info.call(logger3, arguments);
}

var wrap = new InnerLogger();

function getCallStack() {
    var stack = [];
    var i = 0;
    var fun = arguments.callee;
    do {
        fun = fun.arguments.callee.caller;
        ++i;
        stack.push(funStr(fun));
    } while (fun && i<20);
    return stack;

}

var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
function funStr(func) {
    if(!func){
       return "";
    }
    var fnStr = func.toString().replace(STRIP_COMMENTS, '')
    fnStr = fnStr.replace(/[\r\n]/g,"").replace(/\s+/g, "");
    var len = 100;
    if(fnStr.length<len){
        return fnStr;
    }else{
        return fnStr.substring(0,len-2);
    }

}